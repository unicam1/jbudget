package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.*;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class Movement implements IMovement {
    private String id;
    private transient ITransaction transaction;
    private Collection<ITag> tags;
    private Date date;
    private String description;
    private BigDecimal amount;
    private IMovementType movementType;
    private IAccount account;

    public Movement(ITransaction transaction, Collection<ITag> tags, Date date, String description, BigDecimal amount, IMovementType movementType, IAccount account) {
        this.id = UUID.randomUUID().toString();
        this.transaction = transaction;
        this.tags = tags;
        this.date = date;
        this.account = account;
        this.description = description;
        this.amount = amount;
        this.movementType = movementType;
    }

    /**
     * Descrizione opzionale del movimento effettuato
     *
     * @return Stringa che rappresenta la descrizione inserita
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @return Il tipo di movimento
     */
    @Override
    public IMovementType getType() {
        return movementType;
    }

    /**
     * @return Quantità del movimento effettuato
     */
    @Override
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * @return Account dal quale è stato fatto il movimento
     */
    @Override
    public IAccount getAccount() {
        return account;
    }

    public void setAccount(IAccount account) {
        this.account = account;
    }

    /**
     * @return Identificativo univoco del movimento
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * @return Data del movimento
     */
    @Override
    public Date getDate() {
        return date;
    }

    /**
     * @return Lista dei tag associati al movimento
     */
    @Override
    public Collection<ITag> getTags() {
        return tags;
    }

    /**
     * @return La transazione di cui faceva parte il movimento
     */
    @Override
    public ITransaction getTransaction() {
        return transaction;
    }
}
