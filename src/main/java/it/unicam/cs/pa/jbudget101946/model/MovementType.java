package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.IMovementType;

public enum MovementType implements IMovementType {
    DEBITS {
        @Override
        public String getMovementTypeString() {
            return "Debits";
        }
    },
    CREDITS {
        @Override
        public String getMovementTypeString() {
            return "Credits";
        }
    };
}
