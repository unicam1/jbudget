package it.unicam.cs.pa.jbudget101946.view;

import it.unicam.cs.pa.jbudget101946.model.AccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccount;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.ITag;
import it.unicam.cs.pa.jbudget101946.util.Constant;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Menu<T> {
    /**
     * Stampa il menu dell'applicazionee ne richiede la scelta della funzion
     *
     * @return Ritorna un intero per direquale operazione si è scelta
     */
    public static int printMenu() {
        Integer selection;
        Scanner input = new Scanner(System.in);
        System.out.println("Scegliere una delle seguenti operazioni");
        System.out.println("-------------------------\n");
        System.out.println("1 - Aggiungere un conto\n" +
                "2 - Visualizzare gli conto\n" +
                "3 - Aggiungere  un tag\n" +
                "4 - Lista dei tag\n" +
                "5 - Registra una transazione\n" +
                "6 - Leggere le transazione\n" +
                "7 - Esporta dati\n" +
                "8 - Importa dati\n" +
                "9 - Elenco spese ricavi per periodo\n" +
                "10 - Elenco spese ricavi per tag\n" +
                "11 - Elenco spese ricavi per periodo e tag\n" +
                "12 - Exit\n");

        selection = input.nextInt();
        return selection;
    }

    /**
     * Stama a console la richiesta di un account type
     *
     * @return UN tipo di account
     */
    public static IAccountType requestAccountType() {
        AccountType selection = null;
        Scanner input = new Scanner(System.in);
        while (selection == null) {
            System.out.println("Inserire il seguente valore:");
            System.out.println("-------------------------\n");
            System.out.println("1 - Liabilities\n" +
                    "2 - Assets\n" +
                    "3 - Expanses\n" +
                    "4 - Incomes\n");
            Integer temp = input.nextInt();
            switch (temp) {
                case 1:
                    selection = AccountType.LIABILITIES;
                    break;
                case 2:
                    selection = AccountType.ASSETS;
                    break;
                case 3:
                    selection = AccountType.EXPANSES;
                    break;
                case 4:
                    selection = AccountType.INCOMES;
                    break;
                default:
                    break;
            }
        }
        return selection;
    }

    /**
     * RIchiede l'inserimento da tastiera di una stringa
     *
     * @param variableName Il nome del parametro che l'utente deve inserire
     * @return La stringa inserita dall'utente
     */
    public static String requestString(String variableName) {
        String selection;
        Scanner input = new Scanner(System.in);
        System.out.println("Inserire il seguente valore:");
        System.out.println("-------------------------\n");
        System.out.println(variableName + ": ");
        selection = input.nextLine();
        return selection;
    }

    /**
     * Richiede l'inserimento da tastiera di una stringa
     *
     * @param variableName Il nome del parametro che l'utente deve inserire
     * @return Il numero inserito dall'utente
     */
    public static BigDecimal requestBigDecimal(String variableName) {
        BigDecimal selection;
        Scanner input = new Scanner(System.in);
        System.out.println("Inserire il seguente valore:");
        System.out.println("-------------------------\n");
        System.out.println(variableName + ": ");
        selection = new BigDecimal(input.nextFloat());
        return selection;
    }


    /**
     * Richiede da console una data
     *
     * @param variableName Il nome del parametro che l'utente deve inserire
     * @return La data inserita
     */
    public static Date requestDate(String variableName) {
        String dateFormat = "dd-MM-yyyy";
        Date date = null;
        while (date == null) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Inserire una data nel seguente formato " + dateFormat + ":");
            System.out.println("-------------------------\n");
            System.out.println(variableName + ": ");
            try {
                date = new SimpleDateFormat(dateFormat).parse(scanner.nextLine());
            } catch (ParseException e) {
            }
        }
        return date;
    }


    /**
     * Richiede da console la selezione di un account
     *
     * @param accounts Lista di account tra cui scegliere
     * @return Id dell'account scelto
     */
    public static String requestAccount(List<IAccount> accounts) {
        String selection = null;
        Scanner input = new Scanner(System.in);
        while (selection == null) {
            System.out.println("Inserire il seguente valore:");
            System.out.println("-------------------------\n");
            for (Integer i = 0; i < accounts.size(); i++) {
                System.out.println(i + " - " + accounts.get(i).getName() + "\n");
            }

            Integer temp = input.nextInt();
            if (temp < accounts.size()) {
                selection = accounts.get(temp).getId();
            }
        }
        return selection;
    }

    /**
     * Inserita una lista di tag in ingresso, permette all'utente di selezionare da console i tag che vuole inserire
     *
     * @param tags I tag tra i quali scegliere
     * @return Una lista di stringhe
     */
    public static Collection<String> requestMultipleTag(List<ITag> tags) {
        List<String> selection = new ArrayList<>();
        Scanner input = new Scanner(System.in);
        Boolean notExit = true;
        while (notExit) {
            System.out.println("Scegliere un tag da aggiungere oppure inserire "+tags.size()+" per uscire con i soli elementi selezionati:");
            System.out.println("-------------------------\n");
            for (Integer i = 0; i < tags.size(); i++) {
                System.out.println(i + " - " + tags.get(i).getName() + "\n");
            }
            Integer temp = input.nextInt();
            if (temp < tags.size()) {
                selection.add(tags.get(temp).getId());
            }else if(temp == tags.size()){
                notExit = false;
            }else{
                System.out.println("Valore non consentito\n");
            }
        }
        return selection;
    }


    public static void printList(List input){
        if (input == null || input.isEmpty()) {
            System.out.println("------------------- NO RECORD -----------------------------");
        }
        for (int i = 0; i < input.size(); i++) {
            System.out.println("------------------- ELEMENT " + i + " -----------------------------");
            System.out.println(Constant.GSON.toJson(input.get(i)));
            System.out.println("------------------- ELEMENT " + i + " END -----------------------------");
        }
    }
}
