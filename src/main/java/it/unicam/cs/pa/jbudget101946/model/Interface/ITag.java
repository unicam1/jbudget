package it.unicam.cs.pa.jbudget101946.model.Interface;

/**
 * Il tag serve a poter raggruppare e categorizzare i movimenti/transazioni
 */
public interface ITag {
    /**
     * @return Descrizione del tag
     */
    String getDescription();

    /**
     * @return Nome del tag
     */
    String getName();

    /**
     * @return Identificativo del tag
     */
    String getId();
}
