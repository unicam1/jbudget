package it.unicam.cs.pa.jbudget101946.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.unicam.cs.pa.jbudget101946.model.*;
import it.unicam.cs.pa.jbudget101946.model.Interface.*;

public class Constant {
    public  static Gson GSON = new GsonBuilder()
            .registerTypeAdapter(ITag.class, InterfaceSerializer.interfaceSerializer(Tag.class))
            .registerTypeAdapter(ILedger.class, InterfaceSerializer.interfaceSerializer(Ledger.class))
            .registerTypeAdapter(IAccount.class, InterfaceSerializer.interfaceSerializer(Account.class))
            .registerTypeAdapter(IAccountType.class, InterfaceSerializer.interfaceSerializer(AccountType.class))
            .registerTypeAdapter(IMovement.class, InterfaceSerializer.interfaceSerializer(Movement.class))
            .registerTypeAdapter(ITransaction.class, InterfaceSerializer.interfaceSerializer(Transaction.class))
            .registerTypeAdapter(IMovementType.class, InterfaceSerializer.interfaceSerializer(MovementType.class))
            .create();
}
