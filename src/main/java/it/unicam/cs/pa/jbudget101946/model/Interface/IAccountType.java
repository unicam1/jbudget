package it.unicam.cs.pa.jbudget101946.model.Interface;


/**
 * L'implementazione di questa interfaccia sarà una classe, che quando instanziata dovrà descrivere un tipo di account
 */
public interface IAccountType {
    /**
     * @return Il tipo di account sotto forma di stringa leggibile
     */
    String getAccountTypeString();
}
