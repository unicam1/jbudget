package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Ledger implements ILedger {
    private Collection<IAccount> accounts;
    private Collection<ITag> tags = new ArrayList<>();
    private Collection<ITransaction> transactions = new ArrayList<>();


    /**
     * @return La lista dei conti
     */
    @Override
    public Collection<IAccount> getAccounts() {
        return accounts;
    }

    /**
     * Aggiunge un conto
     *
     * @param iAccountType   Il tipo di account
     * @param name           Un nome per visualizzare il conto
     * @param description    Una descrizione del conto
     * @param openingBalance La quantità di soldi
     */
    @Override
    public void addAccount(IAccountType iAccountType, String name, String description, BigDecimal openingBalance) {
        //Se nullo lo istnzio
        if (accounts == null) {
            accounts = new ArrayList<>();
        }

        // 1. Creo il modello account
        Account a = new Account();
        a.setName(name);
        a.setiAccountType(iAccountType);
        a.setDescription(description);
        a.setOpeningBalance(openingBalance);

        // 2. Lo aggiungo al modello
        accounts.add(a);
    }

    /**
     * @param transaction Una transazione
     */
    @Override
    public void addTransaction(ITransaction transaction) {
        this.transactions.add(transaction);
    }

    /**
     * @return Tutte le transazioni
     */
    @Override
    public Collection<ITransaction> getTransactions() {
        return this.transactions;
    }

    /**
     * @param transactionPredicate Predicato per filtrare le transazioni
     * @return La lista delle transazioni filtrate
     */
    @Override
    public Collection<ITransaction> getTransactions(Predicate<ITransaction> transactionPredicate) {
        return this.transactions.parallelStream().filter(transactionPredicate).collect(Collectors.toList());
    }

    /**
     * @return La lista dei tag
     */
    @Override
    public Collection<ITag> getTags() {
        return tags;
    }

    /**
     * @param name        Nome del tag
     * @param description Descrizione del tag
     */
    @Override
    public void addTag(String name, String description) {
        Tag t = new Tag();
        t.setDescription(description);
        t.setName(name);
        t.setId(UUID.randomUUID().toString());
        this.tags.add(t);
    }
}
