package it.unicam.cs.pa.jbudget101946.model.Interface;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * IAccount è l'interfaccia che si occupa di descrivere un conto dell'utente. I tipi di conto che l'utente può avere sono descritti nell'enumeratore AccountType
 */
public interface IAccount {

    /**
     * @return Il nome del conto
     */
    String getName();

    /**
     * @return La descrizione del conto
     */
    String getDescription();

    /**
     * @return Identificativo del conto
     */
    String getId();

    /**
     * @return Saldo all'apertura del conto
     */
    BigDecimal getOpeningBalance();

    /**
     * La somma di tutti i movimenti,e del bilancio iniziare, restituiranno il saldo finale
     *
     * @return Restituisce il bilancio attuale
     */
    BigDecimal getBalance();


    /**
     * @return La lista completa dei movimenti effettuati su questo conto
     */
    Collection<IMovement> getMovements();

    /**
     * @param iMovementPredicate Predicato che permette di filtrare i movimenti
     * @return La lista dei movimenti filtrata secondo il predicato
     */
    Collection<IMovement> getMovements(Predicate<IMovement> iMovementPredicate);
}
