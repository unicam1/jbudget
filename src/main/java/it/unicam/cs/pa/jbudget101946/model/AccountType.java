package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.IAccountType;

public enum AccountType implements IAccountType {
    ASSETS {
        @Override
        public String getAccountTypeString() {
            return "ASSETS";
        }
    },
    LIABILITIES {
        @Override
        public String getAccountTypeString() {
            return "Liabilities";
        }
    },
    EXPANSES {
        @Override
        public String getAccountTypeString() {
            return "Expanses";
        }
    },
    INCOMES {
        @Override
        public String getAccountTypeString() {
            return "Incomes";
        }
    };
}
