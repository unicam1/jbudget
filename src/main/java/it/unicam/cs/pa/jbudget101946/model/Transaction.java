package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.IMovement;
import it.unicam.cs.pa.jbudget101946.model.Interface.ITag;
import it.unicam.cs.pa.jbudget101946.model.Interface.ITransaction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class Transaction implements ITransaction {

    private Date date;
    private String id;
    private Collection<ITag> tags = new ArrayList<>();
    private Collection<IMovement> movements = new ArrayList<>();




    public Transaction() {
        this.id = UUID.randomUUID().toString();
    }

    /**
     * @return Identificativo della transazione
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * @return La lista dei movimenti
     */
    @Override
    public Collection<IMovement> getMovements() {
        return movements;
    }

    /**
     * @return La lista dei tag
     */
    @Override
    public Collection<ITag> getTags() {
        return tags;
    }

    /**
     * Aggiunge un tag
     *
     * @param tag Il tag da aggiungere
     */
    @Override
    public void addTag(ITag tag) {
        this.tags.add(tag);
    }



    /**
     * Rimuove un tag
     *
     * @param tag Il tag da rimuovere, deve essere popolato principalmente l'ID
     */
    @Override
    public void removeTag(ITag tag) {
        this.tags.remove(tag);
    }

    /**
     * @return La data della transazione
     */
    @Override
    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    public void addMovement(IMovement movement) {
        this.movements.add(movement);
    }

}
