package it.unicam.cs.pa.jbudget101946.view;

import it.unicam.cs.pa.jbudget101946.controller.LedgerController;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccount;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.ITag;
import it.unicam.cs.pa.jbudget101946.model.Ledger;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class App {

    public static void main(String[] args) {
        // 1. Definizione delle variabili utilizzate
        Ledger ledger = new Ledger();
        LedgerController ledgerController = new LedgerController(ledger);


        Integer selection = Menu.printMenu();
        while (selection != 12) {
            System.out.println("Choosen voice: " + selection);
            switch (selection) {
                case 1:
                    System.out.println("Aggiungi account -----------------------------------------------------");
                    String name = Menu.requestString("name");
                    String description = Menu.requestString("description");
                    BigDecimal openingAmount = Menu.requestBigDecimal("openingAccount");
                    IAccountType accountType = Menu.requestAccountType();
                    ledgerController.addAccount(ledger, accountType, name, description, openingAmount);
                    break;
                case 2:
                    System.out.println("Visualizza accounts ---------------------------------------------------------------");
                    Menu.printList(ledgerController.listAccount(ledger));
                    break;
                case 3:
                    System.out.println("Aggiungi tag ---------------------------------------------------------------");
                    String tagName = Menu.requestString("name");
                    String tagDescription = Menu.requestString("description");
                    ledgerController.addTags(ledger, tagName, tagDescription);
                    break;
                case 4:
                    System.out.println("Lista tag ---------------------------------------------------------------");
                    Menu.printList(ledgerController.listTags(ledger));
                    break;
                case 5:
                    System.out.println("Aggiungo una transazione ---------------------------------------------------------------");
                    Collection<String> tagsId = Menu.requestMultipleTag((List<ITag>) ledger.getTags());
                    System.out.println("Seleziona l'account da cui i soldi verranno prelevati");
                    String startingAccountId = Menu.requestAccount((List<IAccount>) ledger.getAccounts());
                    System.out.println("Seleziona l'account in cui i soldi andranno a finire");
                    String endingAccountId = Menu.requestAccount((List<IAccount>) ledger.getAccounts());
                    BigDecimal amount = Menu.requestBigDecimal("Importo della transazione");
                    Date date = Menu.requestDate("Data della transazione");
                    String transactionDescription = Menu.requestString("Descrizone della transazione");

                    ledgerController.addTransaction(ledger, tagsId, startingAccountId, endingAccountId, amount, date, transactionDescription);
                    break;
                case 6:
                    Menu.printList(ledgerController.listTransaction(ledger));
                    break;
                case 7:
                    ledgerController.exportData(ledger, Menu.requestString("Path del file dove salvare i dati, compreso di estensione .json (Ex. ledger.json):"));
                    break;
                case 8:
                    ledger = ledgerController.importData(Menu.requestString("Path del file da dove importare i dati, compreso di estensione .json (Ex. ledger.json):"));
                    break;
                case 9:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromPeriod(ledger, Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromPeriod(ledger, Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    break;
                case 10:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromTag(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags())));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromTag(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags())));
                    break;
                case 11:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromTagAndPeriod(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags()), Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromTagAndPeriod(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags()), Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    break;
                default:
                    System.out.println("Nessuna funzione associata al numero " + selection);
                    break;
            }
            selection = Menu.printMenu();
        }
        System.out.println("Exiting...");

    }
}
