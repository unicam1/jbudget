package it.unicam.cs.pa.jbudget101946.model.Interface;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

/**
 * Interfaccia della classe che registra un movimento.
 * Il movimento è una classe che può far parte di una transizione, ha un quantità di soldi in uscita/entrata da un conto.
 * Può essere arricchito con dei tag
 */
public interface IMovement {
    /**
     * Descrizione opzionale del movimento effettuato
     *
     * @return Stringa che rappresenta la descrizione inserita
     */
    String getDescription();

    /**
     * @return Il tipo di movimento
     */
    IMovementType getType();

    /**
     * @return Quantità del movimento effettuato
     */
    BigDecimal getAmount();

    /**
     * @return Account dal quale è stato fatto il movimento
     */
    IAccount getAccount();

    /**
     * @return Identificativo univoco del movimento
     */
    String getId();

    /**
     * @return Data del movimento
     */
    Date getDate();

    /**
     * @return Lista dei tag associati al movimento
     */
    Collection<ITag> getTags();

    /**
     * @return La transazione di cui faceva parte il movimento
     */
    ITransaction getTransaction();

}
