package it.unicam.cs.pa.jbudget101946.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.unicam.cs.pa.jbudget101946.model.*;
import it.unicam.cs.pa.jbudget101946.model.Interface.*;
import it.unicam.cs.pa.jbudget101946.util.InterfaceSerializer;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

public class LedgerController {


    /**
     * Variabile che si occupa di serializzare e deserializzare le variabili in GSON.
     * Serve a impostare quale implementazione dell'interfaccia la libreria GSON debba utilizzare
     */
    private Gson gson = new GsonBuilder()
            .registerTypeAdapter(ITag.class, InterfaceSerializer.interfaceSerializer(Tag.class))
            .registerTypeAdapter(ILedger.class, InterfaceSerializer.interfaceSerializer(Ledger.class))
            .registerTypeAdapter(IAccount.class, InterfaceSerializer.interfaceSerializer(Account.class))
            .registerTypeAdapter(IAccountType.class, InterfaceSerializer.interfaceSerializer(AccountType.class))
            .registerTypeAdapter(IMovement.class, InterfaceSerializer.interfaceSerializer(Movement.class))
            .registerTypeAdapter(ITransaction.class, InterfaceSerializer.interfaceSerializer(Transaction.class))
            .registerTypeAdapter(IMovementType.class, InterfaceSerializer.interfaceSerializer(MovementType.class))
            .create();

    /**
     * Inizializzo di default 4 account per entrate,uscite,prestiti e risparmi
     * Inizializzo anche dei tag di default
     *
     * @param ledger Il libro  mastro iniziale
     */
    public LedgerController(Ledger ledger) {
        // 1. Account default
        ledger.addAccount(AccountType.LIABILITIES, "Prestiti", "Account generato di default", BigDecimal.ZERO);
        ledger.addAccount(AccountType.EXPANSES, "Spese", "Account generato di default", BigDecimal.ZERO);
        ledger.addAccount(AccountType.INCOMES, "Entrate", "Account generato di default", BigDecimal.ZERO);
        ledger.addAccount(AccountType.ASSETS, "Risparmi", "Account generato di default", BigDecimal.ZERO);

        // 2. Tag default
        ledger.addTag("Mobili", "Elementi di arredo");
        ledger.addTag("Auto", "Manutenzioni e componenti di ricambio per i mezzi di trasporto in famiglia");
        ledger.addTag("Utenze", "Affitto e bollette");
        ledger.addTag("Cibo", "Alimenti");
        ledger.addTag("Stipendio", "Entrate ricevute dal lavoro");
    }

    /**
     * Aggiunge un account al libro mastro
     * @param ledger
     * @param iAccountType
     * @param name
     * @param description
     * @param openingAmount
     */
    public void addAccount(Ledger ledger, IAccountType iAccountType, String name, String description, BigDecimal openingAmount) {
        ledger.addAccount(iAccountType, name, description, openingAmount);
    }

    /**
     * Ritorna la lista degli account
     *
     * @param ledger
     * @return
     */
    public List<IAccount> listAccount(Ledger ledger) {
        List<IAccount> temp = (List<IAccount>) ledger.getAccounts();

        return temp;
    }


    /**
     * Permette di creareun nuovo tag nel proprio libro contabile
     *
     * @param ledger      Libro contabile al quale aggiungerlo
     * @param name        Nome del tag
     * @param description Relativa descrizione
     */
    public void addTags(Ledger ledger, String name, String description) {
        ledger.addTag(name, description);
    }


    /**
     * Ritorna la lista dei tag creati relativi al Ledger
     *
     * @param ledger
     * @return
     */
    public List<ITag> listTags(Ledger ledger) {
        List<ITag> temp = (List<ITag>) ledger.getTags();
        return temp;
    }


    /**
     * Aggiunge una transazione
     *
     * @param ledger            Libro mastro a cui aggiungerla
     * @param tagsId            Lista degli  id dei tag, che si vogliono associare alla transazione
     * @param startingAccountId Id dal quale vengono prelevati i soldi
     * @param endingAccountId   Id dell'account dal quale vengono tolti i soldi
     * @param amount            Ammontare della transazione
     * @param date              Data nel formato dd-MM-yyyy
     * @param description       Descrizione della transazione
     */
    public void addTransaction(Ledger ledger, Collection<String> tagsId, String startingAccountId, String endingAccountId, BigDecimal amount, Date date, String description) {
        Transaction t = new Transaction();
        // 1. Aggiungo tutti i tag
        tagsId.parallelStream().forEach(x -> t.addTag(ledger.getTags().parallelStream().filter(y -> y.getId().equals(x)).findFirst().orElse(null)));

        // 2. Creo il  movimento  in uscita
        Movement m = new Movement(t, t.getTags(), date, description, amount, MovementType.DEBITS, ledger.getAccounts().parallelStream().filter(x -> x.getId().equals(startingAccountId)).findFirst().orElse(null));
        // 3. Creo il movimento in entrata
        Movement m2 = new Movement(t, t.getTags(), date, description, amount, MovementType.CREDITS, ledger.getAccounts().parallelStream().filter(x -> x.getId().equals(endingAccountId)).findFirst().orElse(null));

        // 4. Li aggiungo alla transazione
        t.addMovement(m);
        t.addMovement(m2);
        t.setDate(date);

        ledger.addTransaction(t);
    }

    /**
     *
     * @param ledger
     * @return La lista delle transazioni da stampare
     */
    public List<ITransaction> listTransaction(Ledger ledger) {
        return (List<ITransaction>) ledger.getTransactions();
    }


    /**
     * Esporta di dati del software in un file
     *
     * @param ledger Il dato da esportare
     * @param path   Il file in cui esportarlo
     */
    public void exportData(Ledger ledger, String path) {
        try (FileWriter file = new FileWriter(path)) {
            file.write(gson.toJson(ledger));
            file.flush();
        } catch (IOException e) {
            System.out.println("Errore nello scrivere l'export del file. " + e.getMessage());
        }
    }


    /**
     * Import di dati del software in un file
     *
     * @param path Il file da cui importarlo
     */
    public Ledger importData(String path) {
        Ledger output = null;
        try (FileReader reader = new FileReader(path)) {
            output = gson.fromJson(reader, Ledger.class);
        } catch (FileNotFoundException e) {
            System.out.println("Errore File non trovato nel path inserito. Path:" + path + " Error detail: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Errore nella lettura del file. Path:" + path + " Error detail: " + e.getMessage());
            e.printStackTrace();
        }
        return output;
    }

    /**
     *
     * @param ledger
     * @param startDate
     * @param endDate
     * @return Le entrate nel periodo inserito
     */
    public BigDecimal incomesFromPeriod(Ledger ledger, Date startDate, Date endDate) {
        BigDecimal output = BigDecimal.ZERO;
        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (!m.getDate().before(startDate) && !m.getDate().after(endDate)) {
                    if (MovementType.CREDITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }

    /**
     *
     * @param ledger
     * @param startDate
     * @param endDate
     * @return  Le spese nel periodo  inserito
     */
    public BigDecimal expansesFromPeriod(Ledger ledger, Date startDate, Date endDate) {
        BigDecimal output = BigDecimal.ZERO;
        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (!m.getDate().before(startDate) && !m.getDate().after(endDate)) {
                    if (MovementType.CREDITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }


    /**
     *
     * @param ledger
     * @param tags
     * @return Le entrate nel periodo inserito
     */
    public BigDecimal incomesFromTag(Ledger ledger, Collection<String> tags) {
        BigDecimal output = BigDecimal.ZERO;
        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (tags.stream().anyMatch(x -> m.getTags().stream().anyMatch(y -> y.getId().equals(x)))) {
                    if (MovementType.CREDITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }

    /**
     *
     * @param ledger
     * @param tags
     * @return Le uscite nel periodo inserito
     */
    public BigDecimal expansesFromTag(Ledger ledger, Collection<String> tags) {
        BigDecimal output = BigDecimal.ZERO;
        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (tags.stream().anyMatch(x -> m.getTags().stream().anyMatch(y -> y.getId().equals(x)))) {
                    if (MovementType.DEBITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }

    /**
     *
     * @param ledger
     * @param tags
     * @param startDate
     * @param endDate
     * @return Le entrate filtrate per periodo e tag inseriti
     */
    public BigDecimal incomesFromTagAndPeriod(Ledger ledger, Collection<String> tags, Date startDate, Date endDate) {
        BigDecimal output = BigDecimal.ZERO;

        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (tags.stream().anyMatch(x -> m.getTags().stream().anyMatch(y -> y.getId().equals(x))) && !m.getDate().before(startDate) && !m.getDate().after(endDate)) {
                    if (MovementType.CREDITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }

    /**
     *
     * @param ledger
     * @param tags
     * @param startDate
     * @param endDate
     * @return Le Uscite filtrate per periodo e tag inseriti
     */
    public BigDecimal expansesFromTagAndPeriod(Ledger ledger, Collection<String> tags, Date startDate, Date endDate) {
        BigDecimal output = BigDecimal.ZERO;

        for (ITransaction t : ledger.getTransactions()) {
            for (IMovement m :
                    t.getMovements()) {
                if (tags.stream().anyMatch(x -> m.getTags().stream().anyMatch(y -> y.getId().equals(x))) && !m.getDate().before(startDate) && !m.getDate().after(endDate)) {
                    if (MovementType.DEBITS.equals(m.getType())) {
                        output = output.add(m.getAmount());
                    }
                }
            }

        }
        return output;
    }

}
