package it.unicam.cs.pa.jbudget101946.model;

import it.unicam.cs.pa.jbudget101946.model.Interface.IAccount;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.IMovement;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Account implements IAccount {

    private String name;
    private String description;
    private String id;
    private BigDecimal openingBalance;
    private BigDecimal balance;
    private Collection<IMovement> movements;
    private IAccountType iAccountType;

    public Account() {
        this.id = UUID.randomUUID().toString();
    }

    public IAccountType getiAccountType() {
        return iAccountType;
    }

    public void setiAccountType(IAccountType iAccountType) {
        this.iAccountType = iAccountType;
    }

    /**
     * @return Il nome del conto
     */
    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return La descrizione del conto
     */
    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Identificativo del conto
     */
    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return Saldo all'apertura del conto
     */
    @Override
    public BigDecimal getOpeningBalance() {
        return openingBalance;
    }

    public void setOpeningBalance(BigDecimal openingBalance) {
        this.openingBalance = openingBalance;
    }

    /**
     * La somma di tutti i movimenti,e del bilancio iniziare, restituiranno il saldo finale
     *
     * @return Restituisce il bilancio attuale
     */
    @Override
    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    /**
     * @return La lista completa dei movimenti effettuati su questo conto
     */
    @Override
    public Collection<IMovement> getMovements() {
        return movements;
    }

    public void setMovements(Collection<IMovement> movements) {
        this.movements = movements;
    }

    /**
     * @param iMovementPredicate Predicato che permette di filtrare i movimenti
     * @return La lista dei movimenti filtrata secondo il predicato
     */
    @Override
    public Collection<IMovement> getMovements(Predicate<IMovement> iMovementPredicate) {
        return movements.parallelStream().filter(iMovementPredicate).collect(Collectors.toList());
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Account)) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
