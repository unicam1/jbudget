package it.unicam.cs.pa.jbudget101946.model.Interface;

public interface IMovementType {
    /**
     * @return Il tipo di movimento sotto forma di stringa leggibile
     */
    String getMovementTypeString();
}
