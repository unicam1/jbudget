package it.unicam.cs.pa.jbudget101946.model.Interface;

import java.util.Collection;
import java.util.Date;

/**
 * Insieme di movimenti a somma 0
 * Esempio:
 * Faccio un prelievo, scendono i soldi dal conto corrente e salgono i soldi nel conto dei contanti
 */
public interface ITransaction {

    /**
     * @return Identificativo della transazione
     */
    String getId();

    /**
     * @return La lista dei movimenti
     */
    Collection<IMovement> getMovements();

    /**
     * @return La lista dei tag
     */
    Collection<ITag> getTags();

    /**
     * Aggiunge un tag
     *
     * @param tag Il tag da aggiungere
     */
    void addTag(ITag tag);

    /**
     * Rimuove un tag
     *
     * @param tag Il tag da rimuovere, deve essere popolato principalmente l'ID
     */
    void removeTag(ITag tag);

    /**
     * @return La data della transazione
     */
    Date getDate();

}
