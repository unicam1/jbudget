package it.unicam.cs.pa.jbudget101946.model.Interface;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * Rappresenta il libro mastro che tiene traccia di tutti i movimenti
 */
public interface ILedger {
    /**
     * @return La lista dei conti
     */
    Collection<IAccount> getAccounts();

    /**
     * Aggiunge un conto
     *
     * @param iAccountType  Il tipo di account
     * @param name          Un nome per visualizzare il conto
     * @param description   Una descrizione del conto
     * @param openingAmount La quantità di soldi
     */
    void addAccount(IAccountType iAccountType, String name, String description, BigDecimal openingAmount);

    /**
     * @param transaction Una transazione
     */
    void addTransaction(ITransaction transaction);

    /**
     * @return Tutte le transazioni
     */
    Collection<ITransaction> getTransactions();

    /**
     * @param transactionPredicate Predicato per filtrare le transazioni
     * @return La lista delle transazioni filtrate
     */
    Collection<ITransaction> getTransactions(Predicate<ITransaction> transactionPredicate);

    /**
     * @return La lista dei tag
     */
    Collection<ITag> getTags();

    /**
     * @param name        Nome del tag
     * @param description Descrizione del tag
     */
    void addTag(String name, String description);
}