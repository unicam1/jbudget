package it.unicam.cs.pa.jbudget101946;

import it.unicam.cs.pa.jbudget101946.controller.LedgerController;
import it.unicam.cs.pa.jbudget101946.model.AccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccount;
import it.unicam.cs.pa.jbudget101946.model.Interface.IAccountType;
import it.unicam.cs.pa.jbudget101946.model.Interface.ITag;
import it.unicam.cs.pa.jbudget101946.model.Ledger;
import it.unicam.cs.pa.jbudget101946.view.Menu;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class LedgerTest {
    private Ledger ledger;
    private LedgerController ledgerController;

    private static void main() {
        // 1. Definizione delle variabili utilizzate
        Ledger ledger = new Ledger();
        LedgerController ledgerController = new LedgerController(ledger);


        Integer selection = Menu.printMenu();
        while (selection != 15) {
            System.out.println("Choosen voice: " + selection);
            switch (selection) {
                case 1:
                    System.out.println("Aggiungi account -----------------------------------------------------");
                    String name = Menu.requestString("name");
                    String description = Menu.requestString("description");
                    BigDecimal openingAmount = Menu.requestBigDecimal("openingAccount");
                    IAccountType accountType = Menu.requestAccountType();
                    ledgerController.addAccount(ledger, accountType, name, description, openingAmount);
                    break;
                case 2:
                    System.out.println("Visualizza accounts ---------------------------------------------------------------");
                    ledgerController.listAccount(ledger);
                    break;
                case 3:
                    System.out.println("Aggiungi tag ---------------------------------------------------------------");
                    String tagName = Menu.requestString("name");
                    String tagDescription = Menu.requestString("description");
                    ledgerController.addTags(ledger, tagName, tagDescription);
                    break;
                case 4:
                    System.out.println("Lista tag ---------------------------------------------------------------");
                    ledgerController.listTags(ledger);
                    break;
                case 5:
                    System.out.println("Aggiungo una transazione ---------------------------------------------------------------");
                    Collection<String> tagsId = Menu.requestMultipleTag((List<ITag>) ledger.getTags());
                    System.out.println("Seleziona l'account da cui i soldi verranno prelevati");
                    String startingAccountId = Menu.requestAccount((List<IAccount>) ledger.getAccounts());
                    System.out.println("Seleziona l'account in cui i soldi andranno a finire");
                    String endingAccountId = Menu.requestAccount((List<IAccount>) ledger.getAccounts());
                    BigDecimal amount = Menu.requestBigDecimal("Importo della transazione");
                    Date date = Menu.requestDate("Data della transazione");
                    String transactionDescription = Menu.requestString("Descrizone della transazione");

                    ledgerController.addTransaction(ledger, tagsId, startingAccountId, endingAccountId, amount, date, transactionDescription);
                    break;
                case 6:
                    ledgerController.listTransaction(ledger);
                    break;
                case 7:
                    ledgerController.exportData(ledger, Menu.requestString("Path del file dove salvare i dati, compreso di estensione .json (Ex. ledger.json):"));
                    break;
                case 8:
                    ledger = ledgerController.importData(Menu.requestString("Path del file da dove importare i dati, compreso di estensione .json (Ex. ledger.json):"));
                    break;
                case 9:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromPeriod(ledger, Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromPeriod(ledger, Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    break;
                case 10:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromTag(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags())));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromTag(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags())));
                    break;
                case 11:
                    System.out.println("Ricavi nel periodo: " + ledgerController.incomesFromTagAndPeriod(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags()), Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    System.out.println("Spese nel periodo: " + ledgerController.expansesFromTagAndPeriod(ledger, Menu.requestMultipleTag((List<ITag>) ledger.getTags()), Menu.requestDate("Data inizio"), Menu.requestDate("Data fine")));
                    break;
                default:
                    System.out.println("Nessuna funzione associata al numero " + selection);
                    break;
            }
            selection = Menu.printMenu();
        }
        System.out.println("Exiting...");

    }

    @BeforeEach
    public void setUp() {
        ledger = new Ledger();
        ledgerController = new LedgerController(ledger);
    }

    @Test
    public void AddAccountTest() {
        ledgerController.addAccount(ledger, AccountType.LIABILITIES, "TestAccount", "TestDescription", new BigDecimal(1200));
        assertEquals(ledger.getAccounts().size(), 5);
    }

    @Test
    public void ListAccountTest() {
        assertEquals(ledger.getAccounts().size(), ledgerController.listAccount(ledger).size());
    }

    @Test
    public void AddTags() {
        assertEquals(ledger.getTags().size(), 5);
        ledgerController.addTags(ledger, "test1", "test1");
        assertEquals(ledger.getTags().size(), 6);
        for (int i = 0; i < 22; i++) {
            ledgerController.addTags(ledger, "test" + i, "test" + i);
        }
        assertEquals(ledger.getTags().size(), 28);
    }

    @Test
    public void ListTags() {
        assertEquals(ledger.getTags().size(), ledgerController.listTags(ledger).size());
    }

    @Test
    public void AddTransaction() {
        Collection<String> tagsId = new HashSet<>();
        tagsId.add(((List<ITag>) ledger.getTags()).get(0).getId());
        tagsId.add(((List<ITag>) ledger.getTags()).get(3).getId());
        String startingAccountId = ((List<IAccount>) ledger.getAccounts()).get(0).getId();
        String endingAccountId = ((List<IAccount>) ledger.getAccounts()).get(2).getId();
        BigDecimal amount = new BigDecimal(1450);
        Date date = new Date();
        String transactionDescription = "Descrizone della transazione";
        ledgerController.addTransaction(ledger, tagsId, startingAccountId, endingAccountId, amount, date, transactionDescription);
        assertEquals(ledger.getTransactions().size(), 1);
    }

    @Test
    public void ListTransaction() {
        assertEquals(ledger.getTransactions(), ledgerController.listTransaction(ledger));
        Collection<String> tagsId = new HashSet<>();
        tagsId.add(((List<ITag>) ledger.getTags()).get(0).getId());
        tagsId.add(((List<ITag>) ledger.getTags()).get(3).getId());
        String startingAccountId = ((List<IAccount>) ledger.getAccounts()).get(0).getId();
        String endingAccountId = ((List<IAccount>) ledger.getAccounts()).get(2).getId();
        BigDecimal amount = new BigDecimal(1450);
        Date date = new Date();
        String transactionDescription = "Descrizone della transazione";
        ledgerController.addTransaction(ledger, tagsId, startingAccountId, endingAccountId, amount, date, transactionDescription);
        assertEquals(ledger.getTransactions(), ledgerController.listTransaction(ledger));
    }


    @Test
    public void ExportImportData() {
        ledgerController.exportData(ledger, "test.json");
        assertEquals(ledger.getTags().size(), 5);
        ledgerController.addTags(ledger, "test1", "test1");
        assertEquals(ledger.getTags().size(), 6);
        Ledger temp = ledgerController.importData("test.json");
        assertEquals(temp.getTags().size(), 5);
        ledgerController.exportData(ledger, "test.json");
        temp = ledgerController.importData("test.json");
        assertEquals(temp.getTags().size(), 6);
    }

    @Test
    public void IncomesFilter() throws ParseException {
        //Aggiungo una transazione
        Collection<String> tagsId = new HashSet<>();
        tagsId.add(((List<ITag>) ledger.getTags()).get(0).getId());
        tagsId.add(((List<ITag>) ledger.getTags()).get(3).getId());
        String startingAccountId = ((List<IAccount>) ledger.getAccounts()).get(0).getId();
        String endingAccountId = ((List<IAccount>) ledger.getAccounts()).get(2).getId();
        BigDecimal amount = new BigDecimal(1450);
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-06-14");

        String transactionDescription = "Descrizone della transazione";
        ledgerController.addTransaction(ledger, tagsId, startingAccountId, endingAccountId, amount, date, transactionDescription);
        assertEquals(ledger.getTransactions().size(), 1);

        //Filtri
        Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-06-11");
        Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-06-16");
        Collection<String> tagsIdFilter = new HashSet<>();

        assertEquals(ledgerController.incomesFromPeriod(ledger, startDate, endDate), amount);
        assertEquals(ledgerController.expansesFromPeriod(ledger, startDate, endDate), amount);

        tagsIdFilter.add(((List<ITag>) ledger.getTags()).get(1).getId());
        assertEquals(ledgerController.incomesFromTag(ledger, tagsIdFilter), BigDecimal.ZERO);
        assertEquals(ledgerController.expansesFromTag(ledger, tagsIdFilter), BigDecimal.ZERO);


        tagsIdFilter.add(((List<ITag>) ledger.getTags()).get(0).getId());
        tagsIdFilter.add(((List<ITag>) ledger.getTags()).get(3).getId());
        assertEquals(ledgerController.incomesFromTagAndPeriod(ledger, tagsIdFilter, startDate, endDate),amount);
        assertEquals(ledgerController.expansesFromTagAndPeriod(ledger, tagsIdFilter, startDate, endDate), amount);


        // Non ritorna nulla
        startDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-06-11");
        endDate = new SimpleDateFormat("yyyy-MM-dd").parse("2020-06-12");
        assertEquals(ledgerController.incomesFromTagAndPeriod(ledger, tagsIdFilter, startDate, endDate),BigDecimal.ZERO);
        assertEquals(ledgerController.expansesFromTagAndPeriod(ledger, tagsIdFilter, startDate, endDate), BigDecimal.ZERO);



    }
}
